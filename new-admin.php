<?php
session_start();
require_once("koneksi.php");

if (isset($_GET['logout'])) {
  $state = $pdo_con->prepare("DELETE FROM connected where connected_username='" . $_SESSION['me'] . "'");
  $result = $state->execute();

  $state = $pdo_con->prepare("DELETE FROM provider where username='" . $_SESSION['me'] . "'");
  $result = $state->execute();
  if ($result) {
    echo "<script>window.location.href='index.php';</script>";
  }

  insertAction("logoutuser", $_SESSION['me'] . "|" . $_SERVER['REMOTE_ADDR']);
  session_destroy();
}

function insertAction($tipe = "", $aksi = "")
{
  global $pdo_con;
  global $skrg;
  $tipe = ($tipe == "" ? $_GET['type'] : $tipe);
  if ($aksi == "") {
    $aksi = (isset($_GET['action']) ? $_GET['action'] : $_POST['action']);
  }
  $itsme = (isset($_GET['me']) ? $_GET['me'] : $_SESSION['me']);
  $tsession = (isset($_GET['session']) != null ? $_GET['session'] : $_SESSION['session']);
  //Ngecek jika data yang dikirimkan sama seperti sblumnya dan belum terproses
  $state = $pdo_con->prepare("SELECT * FROM user_request where data='" . $aksi . "' AND username='" . $itsme . "' AND username_to='" . $tsession . "'");
  $state->execute();
  $result = $state->fetchAll();
  if (!empty($result)) {
    if ($state->rowCount() > 0) {
      //-1 menandakan apabila sudah ada data yang dikirimkan namun belum terproses.
      echo "-1";
    }
  } else {
    $state = $pdo_con->prepare("INSERT INTO user_request values('" . random(10) . "', '" . $itsme . "', '" .  $tsession . "', '" . $tipe . "','" . $aksi . "', '$skrg')");
    $x = $state->execute();
    echo $x;
  }
}
if (isset($_SESSION['session'])) {
  $state = $pdo_con->prepare("SELECT * FROM provider where username='" . $_SESSION['session'] . "'");
  $state->execute();
  $result = $state->fetchAll();
  $gambar = "/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAABAAEDAREAAhEBAxEB/8QAFAABAAAAAAAAAAAAAAAAAAAAC//EABQQAQAAAAAAAAAAAAAAAAAAAAD/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8AP/B//9k=";
  ?>
<!DOCTYPE html>
<html>

<head>
  <title>Remote Desktop - Administrator - <?php echo $_SESSION['me'] ?></title>
  <script type="text/javascript" src="js/1.3/jquery.min.js"></script>
  <script src="js/1.10.4/jquery-ui.js"></script>
  <script src="js/1.10.4/jquery-ui.js"></script>
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/3.4.1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="lazysizes.min.js" async=""></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="awesome/css/all.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">

</head>
<script>
  var hide = 0;

  function toggleMenu() {
    if (hide == 0) {
      document.getElementsByTagName('li')[0].style = "display:none;";
      document.getElementsByTagName('li')[1].style = "display:none;";
      document.getElementsByTagName('li')[2].style = "display:none;";
      document.getElementsByTagName('li')[3].style = "display:none;";
      document.getElementsByTagName('li')[4].style = "display:none;";
      document.getElementsByTagName('li')[5].style = "display:none;";
      document.getElementsByTagName('li')[6].style = "display:none;";

      document.getElementsByTagName('ul')[0].style = "left:50%;opacity:0.6";


      document.getElementById('toggle').setAttribute("class", "fas fa-angle-down");
      hide = 1;
    } else {
      document.getElementsByTagName('li')[0].style = "";
      document.getElementsByTagName('li')[1].style = "";
      document.getElementsByTagName('li')[2].style = "";
      document.getElementsByTagName('li')[3].style = "";
      document.getElementsByTagName('li')[4].style = "";
      document.getElementsByTagName('li')[5].style = "";
      document.getElementsByTagName('li')[6].style = "";
      document.getElementsByTagName('ul')[0].style = "left:25%";
      document.getElementById('toggle').setAttribute("class", "fas fa-angle-up");
      hide = 0;
    }
  }

  function showModal() {
    $('#myModal').modal('show');
    $('#identity').val('<?= $_SESSION['me']; ?>');
  }
</script>
<style>
  body {
    margin: 0 auto;
    font-family: 'FontAwesome' !important;
    background-color: black;
  }

  .menu {
    position: absolute;
    left: 50%;
    margin: 0 auto;
    color: white;
    background-color: white;
    color: black;
    border-radius: 2px;
  }

  ul {
    transition-duration: 0.5s;
    position: absolute;
    left: 30%;
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: white;
    opacity: 0.8;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    user-drag: none;
    -webkit-user-drag: none;
    -ms-user-select: none;
  }

  li {
    float: left;
    border-right: 1px solid #bbb;
  }

  li:last-child {
    border-right: none;
  }

  li a {
    display: block;
    color: skyblue;
    text-align: center;
    padding: 8px 10px;
    text-decoration: none;
  }

  li a:hover {
    text-decoration: none;
  }

  li:nth-child(7) {
    background-color: red;
  }

  li:nth-child(7) a {
    color: white;
  }

  li a:hover:not(.active) {
    background-color: grey;
  }

  .active {
    background-color: #4CAF50;
  }

  #g {
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    user-drag: none;
    -webkit-user-drag: none;
    -ms-user-select: none;

  }

  #pemberitahuan {
    display: none;
    visibility: hidden;
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: white;
    color: black;
    font-size: 18px;
    font-family: "Segoe UI";
  }

  a:hover {
    cursor: pointer;
  }

  @media only screen and (max-width: 1024px) and (orientation:landscape) {
    html {
      width: 1368px;
      height: 768px;
      /*-moz-transform: scale(0.8, 0.8); /* Moz-browsers */
      */
      /*zoom: 0.8;  Other non-webkit browsers */
      /*zoom: 80%;  Webkit browsers */
    }

    body {
      background-color: black;
      /*background: url('image/test.png');*/
      -webkit-background-size: cover;
      /* For WebKit*/
      -moz-background-size: cover;
      /* Mozilla*/
      -o-background-size: cover;
      /* Opera*/
      background-size: cover;
      width: 1368px;
      height: 768px;
      -moz-transform: scale(1, 1);
      /* Moz-browsers */
      zoom: 1;
      /* Other non-webkit browsers */
      zoom: 100%;
      /* Webkit browsers */

    }

    #g {
      /*display: none;*/
    }

    ul {
      position: fixed;
    }

    a:hover {
      cursor: pointer;
    }
  }

  .lds-ripple {
    display: inline-block;
    position: relative;
    width: 64px;
    height: 64px;
  }

  .lds-ripple div {
    position: absolute;
    border: 4px solid #fff;
    opacity: 1;
    border-radius: 50%;
    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
  }

  .lds-ripple div:nth-child(2) {
    animation-delay: -0.5s;
  }

  @keyframes lds-ripple {
    0% {
      top: 28px;
      left: 28px;
      width: 0;
      height: 0;
      opacity: 1;
    }

    100% {
      top: -1px;
      left: -1px;
      width: 58px;
      height: 58px;
      opacity: 0;
    }
  }

  @media only screen and (max-width: 1024px) and (orientation:portrait) {
    body {
      background-color: white;

    }

    #pemberitahuan {
      display: block;
      visibility: visible;

    }

    #pemberitahuan {
      font-weight: bold;
      top: 20%;
      width: auto;
      height: auto;
    }

    #pemberitahuan img {
      width: 70%;
      max-width: auto;
      max-height: auto
    }

    #g {
      display: none;
      visibility: hidden;
    }

    ul li a {
      display: none;
    }


  }
</style>

<script>
  var present = 0;

  function fe(id) {
    if (id == 1) {
      window.open("window_fe.php", "_blank", "width=700,height=500");
    } else if (id == 2) {
      window.open("window_cmd.php", "_blank", "width=700,height=500");
    } else if (id == 3) {
      window.open("window_chat.php", "_blank", "width=700,height=500");
    } else if (id == 4) {
      presentation();
    }
  }

  function saveNewIdentity() {
    var newname = document.getElementById('identity').value;
    $.post("api.php?conSetName=" + newname, function(data) {
      if (data == "1") {
        window.location.href = 'new-admin.php';
      } else {
        alert('Failed to change name');
      }
    });
  }

  function presentation() {
    if (present == 0) {
      document.getElementsByClassName('nobBinding')[0].style = "";
      document.getElementsByClassName('nobLeft')[0].style = "position:fixed;left:0;min-width:45%;opacity:0.5;height:90%;background-color:black;border:5px solid grey";
      document.getElementsByClassName('nobRight')[0].style = "position:fixed;right:0;min-width:45%;opacity:0.5;height:90%;background-color:grey;border:5px solid black";
      document.getElementsByTagName('li')[3].innerHTML = '<a onclick="fe(4)"><i class="fas fa-eye" style="padding-right:5px;color:skyblue"></i>Normal Mode</a>';
      present = 1;
    } else if (present == 1) {
      document.getElementsByClassName('nobBinding')[0].style = "display:none";
      document.getElementsByTagName('li')[3].innerHTML = '<a onclick="fe(4)"><i class="fas fa-eye" style="padding-right:5px;color:skyblue"></i>Presentation Mode</a>';

      present = 0;
    }
  }
  var left_key = 0;
  var right_key = 1;

  function keydown(key) {
    if (key == left_key) {
      if (bindingMenu == 1) {
        toggleKeyBinding();
      }
      $.get("api.php?sendAction&type=send_keys&action=left", function(result) {});
    } else if (key == right_key) {
      if (bindingMenu == 1) {
        toggleKeyBinding();
      }
      $.get("api.php?sendAction&type=send_keys&action=right", function(result) {});
    }
  }
  var bindingMenu = 1;

  function toggleKeyBinding() {
    if (bindingMenu == 1) {
      document.getElementsByClassName('nobLeft')[0].style = "position:fixed;left:0;min-width:45%;opacity:0.0;height:90%;background-color:black;border:5px solid grey";
      document.getElementsByClassName('nobRight')[0].style = "position:fixed;right:0;min-width:45%;opacity:0.0;height:90%;background-color:grey;border:5px solid black";
    }
  }

  $(document).ready(function() {
    $("#g").on("click", function(event) {
      var x = (event.pageX < 0 ? 0 : event.pageX);
      var y = (event.pageY < 0 ? 0 : event.pageY);
      //alert($('#g').width()+":" + $('#g').height());
      // alert("X Coordinate: " + x + " Y Coordinate: " + y);
      $.get("api.php?sendAction&type=mouse_click&action=" + x + ":" + y, function(result) {});
    });
  });
  $("#g").mousedown(function() {
    return false;
  });

  setInterval(function() {
    $.get("api.php?getScreenshot", function(result) {
      if (result == "exit" || result.indexOf('Undefined index: me in') !== -1) {
        window.location.href = 'index.php';
      } else if (result != "") {
        var xx = new Date();
        datetext = xx.toTimeString();
        datetext = datetext.split(' ')[0];

        $('#update').text(datetext);
        $('#g').attr("src", "data:image/png;base64," + result);
      }
    });
  }, 1200);
</script>


<body onload="toggleMenu()">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><strong>Enter new name for identity : </strong></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" value="" id="identity" name="identity" class="form-control" id="usr">
          </div>
        </div>
        <div class="modal-footer">
          <button style="float:left" onclick="saveNewIdentity()" type="button" class="btn btn-info">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="nobBinding" style="display:none">
    <div class="nobLeft" style="position:fixed;left:0;min-width:45%;opacity:0.5;height:90%;background-color:black;border:5px solid grey" onclick="keydown(left_key)">
      <div style="padding:100px;color:white;font-size:25px;">Click this area to slide previous<br>Click any area to continue</div>
    </div>
    <div class="nobRight" style="position:fixed;right:0;min-width:45%;opacity:0.5;height:90%;background-color:black;border:5px solid grey" onclick="keydown(right_key)">
      <div style="padding:100px;color:white;font-size:25px;">Click this area to next slide</div>
    </div>
  </div>
  <!-- <div class="lds-ripple"><div></div><div></div></div> -->
  <div id="pemberitahuan">
    <center>Please switch to Landscape for use this<br /><br /><img src="image/switchorientation.png"></center>
  </div>
  <ul>
    <li><a onclick="fe(1)"><i class="fas fa-folder" style="padding-right:5px;color:skyblue"></i>File Manager</a></li>
    <li><a onclick="fe(2)"><i class="fas fa-code" style="padding-right:5px;color:skyblue"></i>Command Prompt</a></li>
    <li><a onclick="fe(3)"><i class="fas fa-comment" style="padding-right:5px;color:skyblue"></i>Chat</a></li>
    <li><a onclick="fe(4)"><i class="fas fa-eye" style="padding-right:5px;color:skyblue"></i>Presentation Mode</a></li>
    <li><a href="#"><i class="fas fa-redo-alt" style="padding-right:5px;color:skyblue"></i><span id="update">Last Update</span></a></li>
    <li><a onclick="showModal()"><i class="fas fa-user" style="padding-right:5px;color:skyblue"></i>Change Name</a></li>
    <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Small Modal</button>    -->
    <li><a href="?logout"><i class="fas fa-sign-out-alt" style="padding-right:5px;color:skyblue"></i>End Session</a></li>
    <li style="float:right" onclick="toggleMenu()"><a href="#about"><i id="toggle" class="fas fa-angle-up" style="padding-right:5px;color:skyblue"></i></a></li>
  </ul>

  <div class="isi">
    <center>
      <img id="g" src="data:image/png;base64,<?php echo $gambar ?>">
    </center>
  </div>
  <footer>
  </footer>
</body>

</html>

<?php
} else {
  echo "<script>window.location.href='index.php'</script>";
}
function check_base64_image($base64)
{
  $img = imagecreatefromstring(base64_decode($base64));
  if (!$img) {
    return false;
  }

  imagepng($img, 'tmp.png');
  $info = getimagesize('tmp.png');

  unlink('tmp.png');

  if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
    return true;
  }

  return false;
}
?>