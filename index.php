<?php
date_default_timezone_set('Asia/Jakarta');
require_once("koneksi.php");
session_start();
$notif = "";
$skrg = date("Y-m-d H:i:s", gmmktime());
function insertConnected($con_user, $prov_user){
    global $pdo_con;
    global $skrg;
    $state = $pdo_con->prepare("INSERT INTO connected values(?, ?, ?, ?, ?, ?)");
    $result = $state->execute([$con_user, $prov_user, $skrg, $skrg, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']]);
    //api.php?sendAction&type=fm&action=
}
function connectedLength($connected_username){
    global $pdo_con;
    $state = $pdo_con->prepare("SELECT * FROM connected where connected_username=?");
    $state->execute([$connected_username]);
    $result = $state->fetchAll();
    return $state->rowCount(); 
}
function removeMe($you = ""){
    global $pdo_con;
       //This code was same with logout function

    //Delete connected first
    if (isset($_SESSION['me']) || $you != ""){
        $state = $pdo_con->prepare("DELETE FROM connected where connected_username=?");
        $result  = $state->execute([($you != "" ? $you : $_SESSION['me'])]);
        if ($you == ""){
            $state = $pdo_con->prepare("DELETE FROM provider where username=?");
            $result  = $state->execute([($you != "" ? $you : $_SESSION['me'])]);       
            if ($result){
                echo "<script>window.location.href='index.php';</script>";
            }

            //Control if execute in app
            session_destroy();
        }else{
            echo "OK";
        }

        insertAction(($you != "" || (isset($_SESSION['app'])) ? "logoutuserapp" : "logoutuser"),($you != "" ? $you : $_SESSION['me']). "|" . $_SERVER['REMOTE_ADDR']);
    }
}

// if (isset($_GET['lol'])){
//     global $skrg;
//     echo ("INSERT INTO connected values('". "hehe" . "', '" . "mkjjax" ."', '" . $skrg ."', '" . $skrg ."', '" . $_SERVER['REMOTE_ADDR'] . "')");
// }
if ($pdo_con){
    $sessionexist = (isset($_SESSION['me']) ? connectedLength($_SESSION['me']) : 0);

    if ((!isset($_SESSION['session']) || $_SESSION['session'] == "") || (isset($_SESSION['me']) && $sessionexist <= 0)){
        if ($sessionexist <= 0){
            removeMe();
        }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Remote Desktop</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>

    <script src="js/1.10.4/jquery-ui.js"></script>
    <script src="js/jquery-1.10.2.js"></script>
<script>
function get_browser() {
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE',version:(tem[1]||'')};
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR|Edge\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return M[0] + " " + M[1];
 }

 function get_ip(){
    return (myip);
 }
 
</script>
<script>
$(document).ready(function () {
$("#fff").submit(function(event) {
     var ajaxRequest;
    /* Stop form from submitting normally */
    event.preventDefault();
    var nilai = $("#fff").serialize();
    $.post( "api.php?login&"+nilai+"&browser=" +get_browser()+"&ip="+get_ip(), function( data ) {
        if (data == "0"){
            alert('Password salah');
        }else if (data == "1"){
            alert('Berhasil masuk');
            window.location.href="new-admin.php";
        }
        else if (data == "-1"){
            alert('Username tidak ditemukan');
        }else{
            alert(data);
        }

    });
});
});
</script>
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-b-160 p-t-50">
                <form id="fff" class="login100-form validate-form" action="" method="POST">
                    <span class="login100-form-title p-b-43">
                        Login Remote Acccess
                    </span>
                    <div class="wrap-input100 rs1 validate-input" data-validate = "Username is required">
                        <input class="input100" type="text" name="username">
                        <span class="label-input100">Username</span>
                    </div>
                    
                    
                    <div class="wrap-input100 rs2 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="pass">
                        <span class="label-input100">Password</span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            Sign in
                        </button>
                    </div>

                    <div class="text-center w-full p-t-23">
                        <a href="#" class="txt1">
                            Download Apps
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    

    
    
<!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="js/main.js"></script>
    <!-- Hapus diatas -->

</body>
</html>
<?php
}else{
    echo "<script>alert('you already connected to $_SESSION[session]');window.location.href='new-admin.php';</script>";
}

}
?>