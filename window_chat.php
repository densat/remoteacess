<?php
session_start();
require_once("koneksi.php");
function isPermissionExist($permission){
    //0=only desktop;1=chat;2=file manager;3=command prompt;4=change permission
    global $pdo_con;
    $ok = false;
    if (isset($_SESSION['me'])){
        $state = $pdo_con->prepare("SELECT * FROM connected_permission where connected_username=? AND allow_permission=?");
        $state->execute([$_SESSION['me'], $permission]);
        $result = $state->fetchAll();
        if ($state->rowCount() > 0){
            $ok = true;
        }    
    }else{
        header("location:index.php");
    }
    return $ok;
}
function connectedLength($connected_username){
    global $pdo_con;
    $state = $pdo_con->prepare("SELECT * FROM connected where connected_username=?");
    $state->execute([$connected_username]);
    $result = $state->fetchAll();
    return $state->rowCount(); 
}
// if (isset($_SESSION['me'])&&isPermissionExist("1") &&  connectedLength($_SESSION['me']) > 0){
if (isPermissionExist("1") &&  connectedLength($_SESSION['me']) > 0){
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Chat with <?php echo $_SESSION['session'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="js/1.3/jquery.min.js"></script>
    <script src="js/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.easing-1.4.pack.js"></script>
    <script src="js/jquery-1.10.2.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
    <script src="main.js"></script>
</head>
<style>
tr td:first-child{
    font-weight: bold;
}
table{
    border-collapse: collapse;
}
tr{
    border-bottom: 1px solid black;
    padding-bottom:5px;
}
tr#s{
    border-bottom: 1px solid blue;
    padding-bottom:5px;
}
tr td{
    padding:8px;

}
table{
    width:auto;
    height: auto;
}
div.wrapper {
    overflow:hidden;
    overflow-y: scroll;
    width:100%;
    height: 100%; 
}
</style>
<script>
function pageScroll() {
    $(".wrapper").scrollTop("50");
}
function sendChat(){
    var input = document.getElementsByTagName('input')[0];
    var table = document.getElementsByTagName('table')[0];
    if (event.keyCode == 13) { 
        input.disabled = true;
        $.get("api.php?sendChat&text=" + input.value, function (result) {
                if (result != ""){
                    if (result == "1"){
                        table.innerHTML =  table.innerHTML +  "<tr><td><?= $_SESSION['me'] ?></td><td>" + input.value +"</td></tr>";
                        input.disabled = false;
                        input.value = "";
                        input.focus();

                        durasi=1000;
                        pageScroll();
                    }    
                }
        });
    }else if (event.keyCode == 27) { 
        
    }
}

function loadData(){
    var table = document.getElementsByTagName('table')[0];
    $.get("api.php?getChat", function (result) {
        if (result != ""){     
            var json = $.parseJSON(result);
            if (json.length > 0){
                //alert("Terdapat " + json.length + " pesan");

                for (var i = 0;i < json.length;i++){
                    var from = json[i]['username_from'];
                    var message = json[i]['message'];
                    table.innerHTML =  table.innerHTML +  "<tr id='s'><td>" +  from + "</td><td>" + message +"</td></tr>";
                }
            }
            pageScroll();

        }


    });


    setTimeout(loadData,3000);
}
</script>
<body onload="pageScroll();loadData();">
<div class="wrapper">
<table>
<!-- <tr><td>To</td><td>Hey What are you doing</td></tr> -->
</table>
</div>
<input type="text" style="font-size:20px;width:100%" onkeydown="sendChat()"/>
</body>
</html>
<?php
}else{
    echo "<h1>you dont have permission to access this feature</h1>";
}
?>