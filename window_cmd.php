<?php
session_start();
require_once("koneksi.php");
function isPermissionExist($permission){
    //0=only desktop;1=chat;2=file manager;3=command prompt;4=change permission
    global $pdo_con;
    $ok = false;
    if (isset($_SESSION['me'])){
        $state = $pdo_con->prepare("SELECT * FROM connected_permission where connected_username=? AND allow_permission=?");
        $state->execute([$_SESSION['me'], $permission]);
        $result = $state->fetchAll();
        if ($state->rowCount() > 0){
            $ok = true;
        }
    }else{ 
        header("location:index.php");
    }
    return $ok;
}
function connectedLength($connected_username){
    global $pdo_con;
    $state = $pdo_con->prepare("SELECT * FROM connected where connected_username=?");
    $state->execute([$connected_username]);
    $result = $state->fetchAll();
    return $state->rowCount(); 
}

if (isPermissionExist("3") && connectedLength($_SESSION['me']) > 0){
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Command Prompt (Densat)</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="js/1.3/jquery.min.js"></script>
    <script src="js/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.easing-1.4.pack.js"></script>
    <script src="js/jquery-1.10.2.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
    <script src="main.js"></script>
</head>
<style>
body{
    margin:0 auto;
    background-color:black;
}
.isi{
    
}
.isi input[type="text"]{
    background-color:black;
    color:white;
    border:0;
    width:98.4%;
}
.isi textarea{
    background-color:black;
    color:white;
    border:0;
    width:99.7%;
}
.info{
    padding-top:-10px;
    color:white;
}
.info button{
    margin-right:5px;
}
</style>
<script>
function commandPrompt(){
    if (event.keyCode == 13) { 
        var cmd = data||document.getElementsByName('command')[0].value;
        var out = document.getElementsByName('output')[0];
        $.get("api.php?sendAction&type=cmd&action=" + cmd, function (result) {
                if (result != ""){
                    if (result == "1"){
                        out.value = "Executing command...";
                        durasi=1000;
                        loadData();
                    }    
                }
        });
    }
}
function executeCommand(cmd){
    var out = document.getElementsByName('output')[0];
    $.get("api.php?sendAction&type=cmd&action=" + cmd, function (result) {
            if (result != ""){
                if (result == "1"){
                    out.value = "Executing command...";
                    durasi=1000;
                    loadData();
                }    
            }
    });
}
function getHelp(id){
    if (id == 0){
        executeCommand("ipconfig /all");
    }else if (id ==1){
        var server = prompt("Enter server you want to check");
        server = server.replace("http://");
        server = server.replace("https://");
        executeCommand("ping " + server);
    }else if (id ==2){
        var app_name = prompt("Enter app you want to run (chrome.exe, notepad.exe, calc.exe)");
        if (!app_name.endsWith('.exe')){
            app_name = app_name + ".exe";
        }
        executeCommand(app_name);
    }
}
function pageScroll() {
    var ta = $("#output");
    ta.scrollTop(ta[0].scrollHeight);
}
var durasi = 1000000;
var timeout = 0;
function loadData(){
    var out = document.getElementsByName('output')[0];
    $.get("api.php?getAction", function (result) {
        if (result != ""){     
            var json = $.parseJSON(result);
            if (json['actions'].length > 0){
                // hapus();
                // $("#notification").fadeOut("slow");
                //console.log('json ditemukan');
            }
            for (var i = 0;i < json['actions'].length;i++){
                var single = json['actions'][i];
                if (single['type_data'] == "result_cmd"){
                    out.value = single['data'];
                    durasi = 1000000;
                }else if (single['type_data'] == "session_limit"){
                    window.location.href="new-admin.php";
                }
                pageScroll();
            }
        }


    });


    setTimeout(loadData,durasi);
}
</script>
<body onload="alert('Untuk menjalankan perintah untuk mendapatkan perintah gunakan perintah tanpa .exe, \nContoh : ping 8.8.8.8\nSebaliknya jika hanya menjalankan aplikasi saja gunakan .exe\nContoh : notepad.exe');">
    <div class="isi">
        <div style="float:left;background-color:black;color:white;font-size:14p;width:10px">></div>
        <div style="float:left;width:98%">
            <input onKeyDown="commandPrompt()" style="float:left" type="text" name="command"/>
        </div>
        
        <br>
        <div class="info">Help Command : 
        <button onclick="getHelp(0)">Check network details</button><button onclick="getHelp(1)">Ping server</button><button onclick="getHelp(2)">Open Apps</button></div>
        <textarea id="output" name="output" rows="30">

        </textarea>
    </div>
    
</body>
</html>
<?php
}else{
    echo "<h1>you dont have permission to access this feature</h1>";
}
?>