<?php
session_start();
require_once("koneksi.php");
function isPermissionExist($permission){
    //0=only desktop;1=chat;2=file manager;3=command prompt;4=change permission
    global $pdo_con;
    $ok = false;
    if (isset($_SESSION['me'])){
        $state = $pdo_con->prepare("SELECT * FROM connected_permission where connected_username=? AND allow_permission=?");
        $state->execute([$_SESSION['me'], $permission]);
        $result = $state->fetchAll();
        if ($state->rowCount() > 0){
            $ok = true;
        } 
    }else{
        header("location:index.php");
    }
    return $ok;
}
function connectedLength($connected_username){
    global $pdo_con;
    $state = $pdo_con->prepare("SELECT * FROM connected where connected_username=?");
    $state->execute([$connected_username]);
    $result = $state->fetchAll();
    return $state->rowCount(); 
}
if (isPermissionExist("2")&& connectedLength($_SESSION['me']) > 0){
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>File Manager - Densat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<script type="text/javascript" src="js/1.3/jquery.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>
    <script src="js/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="/fancybox/jquery.easing-1.4.pack.js"></script>
    <!--<script src="js/jquery-1.10.2.js"></script>-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
    <script src="main.js"></script>
</head>
<style>
body{
    margin:0 auto;
    background-color:#191919;
    color:white;
    font-style:"Microsoft Sans Serif";
}
.garis{
    padding-top:8px;
    margin-left:5px;
    
    margin-right:5px;
    border-left:2px double grey;
}
.imghover:hover{
    transition-duration: 0.5s;
    background-color:grey
}
input[type="text"]{
    color:white;
    font-size:14px;
    padding:2px;
}
.folderleft-top{
    margin-top:25px;

}
.folder-text-top{
    transition-duration: 0.5s;
}
.folder-text img{
    float:left;
    margin-right:8px;
    margin-left:25px;
}
.folder-text:hover, .folder-text-top:hover{
    transition-duration: 0.5s;
    background-color:grey;
}
.folder-text, .folder-text-top{
    padding:2px;
    padding-top:5px;
    padding-bottom:5px;
    cursor:pointer;
}
.folder-text-top img{ 
    float:left;
    margin-right:8px;
    margin-left:22px;
}
table{
    max-height:295px;
   /* margin-left:100px;*/
    border-collapse: collapse;
    table-layout:fixed
}
table th{
    border-right:2px solid grey;
    text-align: left;
    font-size:12px;
    padding-left:5px;
}
table th:hover{
    transition-duration:0.99s;
    background-color:grey;
}
table td:first-child{
    padding:6px;padding-left:15px;
}
table td{
    padding-left:5px;
    font-size:12px;
    cursor: initial;
    word-wrap: break-word;
}
table tr:not(:first-child):hover{
    background-color:grey;
    cursor:pointer;
}
td img{
    float:left;
    margin-right:4px;
    vertical-align:text-middle;
    margin-top:-5px;
}
tbody {    /* Just for the demo          */
    overflow:auto;
}
#notification {
    position:fixed;
    top:0px;
    width:100%;
    z-index:105;
    text-align:center;
    font-weight:normal;
    font-size:14px;
    font-weight:bold;
    color:white;
    background-color:#FF7800;
    padding:5px;
}
#notification span.dismiss {
    border:2px solid #FFF;
    padding:0 5px;
    cursor:pointer;
    float:right;
    margin-right:10px;
}
#notification a {
    color:white;
    text-decoration:none;
    font-weight:bold
}
</style>

<script>
var currentDir = "";
var previousDir = "";
var durasi = 1000000;
function inputList(){
    if (event.keyCode == 13) {
        var d = document.getElementsByName('dir')[0].value;
        listing(d.replace("+", "tamb4h"));
    }
    
}
function listing(d){
    $("#notification").fadeIn("slow").text('Waiting get list...');
    durasi=1000;
    loadData();
    //alert(durasi);
    var parts = d.split("\\");
    d = parts.join("/");
    $.get("api.php?sendAction&type=fm&action=" + d, function (result) {
                    if (result != ""){

                    }
    });

}
function awal(){
    
    var cl1 = document.getElementsByClassName('folderleft-top')[0];
    var cl = document.getElementsByClassName('folderleft-top')[1];
    cl1.style="display:none";
    cl.style="display:none";
    listing("drive");
    $.get("api.php?sendAction&type=get_user&action=get_user", function (result) {
        if (result != ""){
            //alert('Mendapatkan username');
        }
    });
    
}

function hapus(){
    $('tr:not(:first-child)').remove();
}
var curr = null;
$(document).on("click", "table tr", function(e) {
    //alert(currentDir.replace("+", "tamb4h") + "\\" + this.children[0].innerText.replace("+", "tamb4h"));
    //listing(currentDir.replace("+", "tamb4h") + "\\" + this.children[0].innerText.replace("+", "tamb4h"));
    //previousDir = currentDir;
    if (curr != null){
        curr.trigger('contextmenu:hide');
    }else{
        console.log('Uhu');
    }
    // alert(this.children[0].innerText);
    var ll = currentDir.replace("+", "tamb4h") + "\\" + this.children[0].innerText.replace("+", "tamb4h");
    
    if (ll.endsWith(".zip") || ll.endsWith(".rar") || ll.endsWith(".mp4") || ll.endsWith(".mkv") || ll.endsWith(".jpeg") || ll.endsWith(".jpg")|| ll.endsWith(".png")|| ll.endsWith(".pptx") || ll.endsWith(".doc") || ll.endsWith(".docx") || ll.endsWith(".xls")|| ll.endsWith(".xlsx")){
        // alert('this is file');
    }else{
        $(this).contextmenu(false);
        listing(currentDir.replace("+", "tamb4h") + "\\" + this.children[0].innerText.replace("+", "tamb4h"));
        previousDir = currentDir;
    }
});
// $(function() {
//     $.contextMenu({
//         selector: 'table tr', 
//         trigger:'left',
//         callback: function(key, options) {
//             var m = "clicked: " + key;
//             //alert($(this).children()[0].innerText.replace("+", "tamb4h").endsWith('.zip'));
//             listing(currentDir.replace("+", "tamb4h") + "\\" + $(this).children()[0].innerText.replace("+", "tamb4h"));
//             //alert();
//             window.console && console.log(m) || alert(m); 
//         },
//         items: {
//             "Download": {name: "download", icon: "file"},
//             "Open in Provider": {name: "download", icon: "file"},
//             "sep1": "---------",
//             "quit": {name: "Quit", icon: function(){
//                 return 'context-menu-icon context-menu-icon-quit';
//             }}
//         }
//     });

//     $('table tr').on('click', function(e){
//         // console.log('clicked', this);
//         console.log('clicked');
//     })    
// });

$(function(){
    $.contextMenu({
        selector: 'table tr', 
        trigger:'left',
        autoHide:true,
        build: function($trigger, e) {

            // this callback is executed every time the menu is to be shown
            // its results are destroyed every time the menu is hidden
            // e is the original contextmenu event, containing e.pageX and e.pageY (amongst other data)
            return {
                callback: function(key, options) {
                    var m = "clicked: " + key;
                    if (key == "Download"){

                        listing(currentDir.replace("+", "tamb4h") + "\\" + $(this).children()[0].innerText.replace("+", "tamb4h"));
                        previousDir = currentDir;
                    }else{
                        $("#notification").fadeIn("slow").text('Connecting to provider...');
                        var dzx = currentDir.replace("+", "tamb4h") + "\\" + $(this).children()[0].innerText.replace("+", "tamb4h");
                        var parts = dzx.split("\\");
                        dzx = parts.join("/");
                            $.get("api.php?sendAction&type=open_file&action=" + dzx, function (result) {
                            if (result != ""){
                                $("#notification").fadeOut("slow");
                            }
                            });
                    }
                    

                },
                items: {
                    "Download": {name: "download"},
                    "Open to PC": {name: "open"},
                    "sep1": "---------"
                    // "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
                }
            };
        }
    });
});

//oncontextmenu=

function ngeback(){
    var za = previousDir.split("\\");
    var kiri = za[za.length-1];
    alert(kiri + "\n" + previousDir + "\n" +za.length);
    //listing();
}

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};
function loadData(){
     $.get("api.php?getAction", function (result) {
        var tab = document.getElementsByTagName('tbody')[0];
        var cl1 = document.getElementsByClassName('folderleft-top')[0];
        var cl = document.getElementsByClassName('folderleft-top')[1];

        if (result != ""){
            var json = $.parseJSON(result);
            if (json['actions'].length > 0){
                // hapus();
                // $("#notification").fadeOut("slow");
            }
            for (var i = 0;i < json['actions'].length;i++){
                var c = json['actions'][i];
                var tipe = (c['type_data']).toString();
                var data = (c['data']).toString();
                var parts = data.split('\\');
                var output = parts.join('\\\\');
                if (tipe == "result_user"){
                    cl1.style="";
                    username_root = output.split('=')[1];
                    //<div class="folder-text" id="downloads"><img src="gambar/download.png" width="18" height="18"/>Downloads</div>
                    cl1.innerHTML = cl1.innerHTML+ "<div class='folder-text' id='downloads' onclick=" + "listing('C:\\\\users\\\\" + username_root+"\\\\Downloads')" +"><img src='gambar/download.png' width='18' height='18'/>Downloads</div>";
                    cl1.innerHTML = cl1.innerHTML+ "<div class='folder-text' id='documents' onclick=" + "listing('C:\\\\users\\\\" + username_root+"\\\\Documents')" +"><img src='gambar/documents.png' width='18' height='18'/>Documents</div>";
                    cl1.innerHTML = cl1.innerHTML+ "<div class='folder-text' id='pictures' onclick=" + "listing('C:\\\\users\\\\" + username_root+"\\\\Pictures')" +"><img src='gambar/picture.png' width='18' height='18'/>Pictures</div>";

                }
                if (output.indexOf("emptydir") !== -1){
                    $("#notification").fadeOut("slow");
                    durasi = 100000;
                    //default = yellow
                    //custom = read for function alert
                    //$("#notification").fadeIn(1500).text('File or directory not found');
                }
                else if (output.startsWith("file=")){
                    var win = window.open(output.split('=')[1].replace("tamb4h", "+"), '_blank');
                    win.focus();
                    $("#notification").fadeOut("slow");
                    durasi = 100000;
                }else{
                    hapus();
                    $("#notification").fadeOut("slow");
                    if (tipe == "result_fm" || tipe == "result_drive"){
                        var json2 = $.parseJSON(output);
                        //console.log(output);
                        for (var x = 0;x < json2.length;x++){
                            var dd = json2[x];
                            var gambar = "";
                            if (dd.type.toString()=="Directory"){
                                gambar  = "cant-folder.png";
                            }else if (dd.type.toString()=="File"){
                                gambar  = "file.png";
                            }
                            if (dd.type.toString()=="Directory" || dd.type.toString()=="File"){
                                tab.innerHTML = tab.innerHTML + '<tr><td><img src="gambar/' + gambar +  '" width="22px" height="22px"/>' + dd.name.replace("tamb4h", "+") + '</td><td>' + dd.date_modified.split('|').join('/') + '</td><td>' + dd.type + '</td><td>' + (dd.type.toString() == "File" ? bytesToSize(dd.Size) : "") + '</td></tr>';
                                if (x == json2.length-1){
                                    currentDir = dd.absolute_path;


                                    document.getElementsByTagName('input')[0].setAttribute('value', dd.absolute_path);
                                }
                            }else if (dd.type.toString()=="Drive"){
                                cl.innerHTML = cl.innerHTML+"<div class=\"folder-text\" onclick=\"listing('" + dd.name.toString().replace("\\", "\\\\") + "')\"><img src='gambar/drivee.png' width=\"18\" height=\"18\"/>Local Disk " +dd.name + "</div>";

                            }
                        }

                        cl.style="";
                    }
                    durasi = 100000;
                }
        //         <tr>
        // <td><img src="gambar/pdf.png" width="22px" height="22px"/>Konten laku 2019.pdf</td>
        // <td>1/7/2019 1:43</td>
        // <td>Archive</td>
        // <td>247,00KB</td>
        // </tr>

            }
            
        }
    });
    
    setTimeout(loadData,durasi);
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}
</script>
<body onload="awal()">
<div id="notification" style="display: none;">
      <span class="dismiss"><a title="dismiss this notification">x</a></span>
    </div>
    <img src="gambar/monitor.png" width="24" height="24"/><span class="garis"></span>
    <img src="gambar/file-check.png" width="24" height="24"/><span class="garis"></span>
    <img src="gambar/cant-folder.png" width="24" height="24"/><span class="garis"></span>
    <span >This PC</span>
    <div class="container">
    <div style="margin-top:6px;" class="topcon">
        <div class="left" style="float:left;margin-right:3px;">
            <img src="gambar/kiri.png" width="20" height="20" onclick="ngeback()" id="back"/>
            <img src="gambar/kanan.png" width="20" height="20" id="forward"/>
            <img class="imghover" src="gambar/atas.png" width="20" height="20"/>
        </div>
        <input name="dir" onKeyDown="inputList()" type="text" style="width:78%;background-color:black;border:1px solid grey;font-size:16px;">
        <input type="hidden" style="width:12%;background-color:black;border:1px solid grey;font-size:16px;" placeholder="Search this PC">
    </div>
    <div style="margin-top:5px;height:100vh" class="mainbody">
        <div style="background-color:#191919;float:left;width:20%;height:100vh;border-right:0.2px solid grey;" class="left">
            <div class="folderleft-top">
                <div class="folder-text-top">
                    <img src="gambar/star.png" width="18" height="18"/>
                    Quick Access
                </div>
                <!-- <div class="folder-text" id="downloads">
                    <img src="gambar/download.png" width="18" height="18"/>
                    Downloads
                </div>
                <div class="folder-text" id="documents">
                    <img src="gambar/documents.png" width="18" height="18"/>
                    Documents
                </div>
                <div class="folder-text" id="pictures">
                    <img src="gambar/picture.png" width="18" height="18"/>
                    Pictures
                </div> -->
            </div>
            <div class="folderleft-top">
                <div class="folder-text-top">
                    <img src="gambar/drive.png" width="18" height="18"/>
                    Drive
                </div>
                <!-- <div class="folder-text" onclick="listing('C:\\')">
                    <img src="gambar/drivee.png"  width="18" height="18"/>
                    Local Disk C
                </div>
                <div class="folder-text"  onclick="listing('D:\\')">
                    <img src="gambar/drivee.png" width="18" height="18"/>
                    Local Disk D
                </div> -->
            </div>
        </div>
        <div style="background-color:#202020;width:79.9%;height:100vh;overflow-y: auto; " class="right">
            <table width="60%">
                <tr>
                    <th style="padding:6px;padding-left:15px;" width="50%">Name</th>
                    <th>Date Modified</th>
                    <th>Type</th>
                    <th>Size</th>
                </tr>
                
            </table>
        </div>
    </div>

    </div>
    
</body>
</html>
<?php
}else{
    echo "<h1>you dont have permission to access this feature</h1>";
}
?>