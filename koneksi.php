<?php
$database_username = 'root';
$database_password = '';
$pdo_con = null;
try {
    $pdo_con = new PDO( 'mysql:host=localhost;dbname=rd', $database_username, $database_password );
} catch (PDOException $e) {
    print "Error!";
}


function random($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>