<?php
require_once("koneksi.php");
session_start();
date_default_timezone_set('Asia/Jakarta');
$notif = "";
$skrg = date("Y-m-d H:i:s", gmmktime());
// if (isset($_GET['ulala'])){

// }

function uploadFile()
{
    if (!isset($_SESSION['me']) || isset($_SESSION['me']) == "") {
        $rDir = random(11);
        $target_dir = "cache/" . $rDir . "/";
        mkdir($target_dir, 0777, true);
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if ($imageFileType == "php" || $imageFileType == "asp" || $imageFileType == "aspx" || $imageFileType == "html" ||  $imageFileType == "php") {
            $target_file = $target_file . ".txt";
        }
        // Check if image file is a actual image or fake image
        // $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        // if($check !== false) {
        //     //echo "File is an image - " . $check["mime"] . ".";
        //     $uploadOk = 1;
        // } else {
        //     echo "File is not an image.";
        //     $uploadOk = 0;
        // }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        // if ($_FILES["fileToUpload"]["size"] > 5000000) {
        //     echo "Sorry, your file is too large.";
        //     $uploadOk = 0;
        // }
        // Allow certain file formats
        // if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        // && $imageFileType != "gif" ) {
        //     echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        //     $uploadOk = 0;
        // }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo str_replace("+", "tamb4h", $target_file) . " has been uploaded.";
            } else {
                echo "-1";
            }
        }
    } else {
        echo "sorry ";
    }
}

if (isset($_GET['upload'])) {
    uploadFile();
}
if (isset($_GET['form'])) {
    echo '<!DOCTYPE html>
<html>
<body>

<form action="api.php?upload" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>

</body>
</html>';
}
if (isset($_GET['ccon'])) {
    if (isset($_GET['uname'])) {
        echo "(" . connectedLength($_GET['uname']) . ")=>$_SESSION[me]";
    } else {
        if (isset($_SESSION['me'])) {

        } else {
            
        }
    }
}
if (isset($_GET['conSetName'])) {
    echo changeNameConnected($_GET['conSetName']);
}
if (isset($_GET['cobadulu'])) {
    insertActionwithCustomSender($_GET['cobadulu'], "login_required", "login required");
}
if (isset($_GET['cperm'])) {
    createPermission();
}
if (isset($_GET['cperm2'])) {
    echo isPermissionExist("0");
}
if (isset($_GET['cperm3'])) {
    echo createPermission("6", "fmtMApZTRS", "559690");
}
if (isset($_GET['cperm4'])) {
    echo removePermission("0", "RkGgHKOgIO", "559690");
}
if (isset($_GET['sendChate'])) {
    if (isset($_GET['text'])) {
        echo sendChat($_GET['text']);
    }
}
if (isset($_GET['remove'])) {
    removeMe();
}
if (isset($_GET['testperm'])) {
    echo (getProviderPermission());
}
if (isset($_GET['provider'])) {
    if (isset($_GET['user']) && isset($_GET['pass']) && !isset($_POST['screen'])) {
        insertConnected($_GET['user'], $_GET['pass'], $_GET['permission']);
    } else {
        $ns = str_replace(" ", "+", $_POST['screen']);
        $state = $pdo_con->prepare("UPDATE provider set screen='" . $ns . "' where username='" . $_GET['user'] . "'");
        $result = $state->execute();
        if ($result) {
            echo "Sent :)";
        } else {
            echo "Unsent :(";
        }
    }
} else if (isset($_GET['getPermission'])) {
    if (isset($_GET['user'])) {
        $provider = (isset($_GET['to']) ? $_GET['to'] : "");
        $dt = getPermission($_GET['user'], $provider);
        $are = [];
        foreach ($dt as $row) {
            array_push($are, $row['allow_permission']);
        }
        echo implode("&", $are);
    }
} else if (isset($_GET['setPermission'])) {
    //createPermission("6","fmtMApZTRS", "559690");
    if (isset($_GET['permission']) && isset($_GET['id']) && isset($_GET['to'])) {
        createPermission($_GET['permission'], $_GET['id'], $_GET['to']);
    } else {
        echo "Kurang lengkap";
    }
} else if (isset($_GET['removePermission'])) {
    //createPermission("6","fmtMApZTRS", "559690");
    if (isset($_GET['permission']) && isset($_GET['id']) && isset($_GET['to'])) {
        removePermission($_GET['permission'], $_GET['id'], $_GET['to']);
    } else {
        echo "Kurang lengkap";
    }
} else if (isset($_GET['getConnected'])) {
    //Jika terdapat lebih dari 0 connector, maka screen ditampilkan putar balik dari netbean ke php
    //Data yang ditampilkan adalah : connected_username:ip_address:first_online_date:user_refresh_date
    if (isset($_GET['user'])) {
        $state = $pdo_con->prepare("SELECT * FROM connected where provider_username='" . $_GET['user'] . "'");
        $state->execute();
        $result = $state->fetchAll();
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $row) {
                echo $row['connected_username'] . "&" . $row['ip_address'] . "&" . $row['first_online_date'] . "&" . $row['user_refresh_date'] . "&" . $row['useragent'];
                $i++;
                if ($i == $state->rowCount()) { } else {
                    echo ";";
                }
            }
        }
    }
} else if (isset($_GET['getScreenshot'])) {
    $state1  = $pdo_con->prepare("SELECT * FROM connected where connected_username='" . $_SESSION['me'] . "';");
    $state1->execute();
    $result1 = $state1->fetchAll();
    //echo "SELECT * FROM connected where connected_username='" . $_SESSION['me'] . "';<br>        ";
    // print_r($result1);
    // print $state1->rowCount();
    //
    if (!empty($result1)) {
        if ($state1->rowCount() > 0) {

            $state  = $pdo_con->prepare("SELECT * FROM provider where username='" . $_SESSION['session'] . "';");
            $state->execute();
            $result = $state->fetchAll();

            echo (isset($_SESSION['screen']) && $_SESSION['screen'] != null ? $_SESSION['screen'] : "");
            if (isset($_SESSION['screen'])) {
                if ($result[0]['screen'] != $_SESSION['screen']) {
                    $_SESSION['screen'] = $result[0]['screen'];
                    echo $result[0]['screen'];
                } else { }
            }
        } else {
            insertAction("logoutuser", $_SESSION['me'] . "|" . $_SERVER['REMOTE_ADDR']);
            $state = $pdo_con->prepare("DELETE FROM connected where connected_username='" . $_SESSION['me'] . "'");
            $result = $state->execute();

            $state = $pdo_con->prepare("DELETE FROM provider where username='" . $_SESSION['me'] . "'");
            $result = $state->execute();
            if ($result) {
                ob_start(); //this should be first line of your page
                //header('Location: index.php');
                echo "<script>window.location.href='index.php');</script>";
                ob_end_flush(); //this should be last line of your page
            }

            session_destroy();
        }
    } else {
        insertAction("logoutuser", $_SESSION['me'] . "|" . $_SERVER['REMOTE_ADDR']);
        $state = $pdo_con->prepare("DELETE FROM connected where connected_username='" . $_SESSION['me'] . "'");
        $result = $state->execute();

        $state = $pdo_con->prepare("DELETE FROM provider where username='" . $_SESSION['me'] . "'");
        $result = $state->execute();
        if ($result) {
            ob_start(); //this should be first line of your page
            // header('Location: index.php');
            echo "<script>window.location.href='index.php');</script>";
            ob_end_flush(); //this should be last line of your page
        }

        session_destroy();
    }
    if (isset($_SESSION['session']) && $_SESSION['session'] != "" && connectedLength($_SESSION['me']) > 0) {
        $state  = $pdo_con->prepare("SELECT * FROM provider where username='" . $_SESSION['session'] . "';");
        $state->execute();
        $result = $state->fetchAll();

        if (!isset($_SESSION['screen'])) {
            $_SESSION['screen'] = "";
        }
        if ($result[0]['screen'] != $_SESSION['screen']) {
            $_SESSION['screen'] = $result[0]['screen'];
            echo $result[0]['screen'];
        }
    } else {
        echo "exit";
        removeMe();
    }
} else if (isset($_GET['getAction'])) {
    $username = (isset($_GET['username']) ? $_GET['username'] : $_SESSION['me']);
    //Ada ga ya aksi utkku
    $data['actions'] = [];
    $state = $pdo_con->prepare("SELECT * FROM user_request where username_to ='" . $username . "'");
    $state->execute();
    $result = $state->fetchAll();
    // if (isset($_SESSION['me'])){
    //     if (connectedLength($_SESSION['me']) > 0){
    //         $data['actions'][] = ['type_data' => 'session_limit'];       
    //     }
    // }
    if (!empty($result)) {
        if ($state->rowCount() > 0) {
            foreach ($result as $row) {
                $data['actions'][] = [
                    'id_ur' => $row['id_ur'], 'username' => $row['username'], 'username_to' => $row['username_to'], 'type_data' => $row['type_data'], 'data' => $row['data'], 'date' => $row['date']
                ];
                $state2 = $pdo_con->prepare("DELETE FROM user_request where id_ur='" . $row['id_ur'] . "'");
                $rez = $state2->execute();
            }
        }
    } else {
        //echo "Kosong";
    }
    // print_r($data);
    // echo "<br>==========================<br>";
    echo json_encode($data);
} else if (isset($_GET['sendChat'])) {
    if (isset($_GET['text'])) {
        sendChat($_GET['text']);
        //removeAll('559690');
    }
} else if (isset($_GET['getChat'])) {
    $re = getChat();
    echo $re;
} else if (isset($_GET['deleteConnected'])) {
    if (isset($_GET['id'])) {
        removeMe($_GET['id']);
        //removeAll('559690');
    }
} else if (isset($_GET['deleteAllConnected'])) {
    if (isset($_GET['id'])) {
        removeAll($_GET['id']);
    }
} else if (isset($_GET['sendAction'])) {
    //api.php?type=newuser&aksi=username
    insertAction();
} else if (isset($_GET['login'])) {
    //Bagian ini untuk versi web
    if (isset($_GET['username']) && isset($_GET['pass']) && isset($_GET['browser'])  && isset($_GET['ip'])) {
        global $koneksi;
        $username = $_GET['username'];
        $password = $_GET['pass'];
        $browser = $_GET['browser'];
        $ip = $_GET['ip'];
        $state  = $pdo_con->prepare("SELECT * FROM provider where username='" . $username . "';");
        $state->execute();
        $result = $state->fetchAll();
        $notif = "";
        if (!empty($result)) {
            foreach ($result as $row) {
                if ($row['password'] == $password) {
                    $con_user = random();
                    $prov_user = $username;
                    $_SESSION['session'] = $prov_user;
                    $_SESSION['me'] = $con_user;

                    if (insertCon($con_user, $prov_user, $browser, $ip)) {
                        insertAction("newuser", $con_user . "|" . $ip);
                        $per = getProviderPermission();
                        $pers = explode(",", $per);
                        for ($i = 0; $i < count($pers); $i++) {
                            createPermission($pers[$i]);
                        }
                        $notif = "<script>window.location.href='new-admin.php';</script>";

                        //echo "1";
                    } else {
                        echo "-99999";
                    }
                    //1

                } else {
                    //0
                    $notif = "Wrong password";
                    echo "0";
                }
            }
        } else {
            //-1
            //$notif =  "Username not found";
            echo "-1";
            //echo $notif;
        }
    }
}
function NOW($format = 'Y-m-d H:i:s')
{
    return date($format);
}
function getChat()
{
    global $pdo_con;
    $auth = false;
    if (isset($_GET['me']) && isset($_GET['to'])) {
        $auth = false;
    } else if (isset($_SESSION['me']) && connectedLength($_SESSION['me']) > 0) {
        $auth = true;
    }
    $state = $pdo_con->prepare("SELECT * FROM chat where username_from=? AND username_to=?");
    $state->execute([($auth ? $_SESSION['session'] : $_GET['to']), ($auth ? $_SESSION['me'] : $_GET['me'])]);
    $result = $state->fetchAll();

    $pesan = ($state->rowCount() > 0 ? $result[0]['message'] : "");

    $state = $pdo_con->prepare("DELETE FROM chat where username_from=? AND username_to=?");
    $state->execute([($auth ? $_SESSION['session'] : $_GET['to']), ($auth ? $_SESSION['me'] : $_GET['me'])]);


    return (!isset($_GET['plain']) ? json_encode($result) : $pesan);
}
function sendChat($chat)
{
    global $pdo_con;
    $auth  = false;
    if (isset($_GET['me']) && isset($_GET['to'])) {
        $auth = false;
    } else if (isset($_SESSION['me']) && connectedLength($_SESSION['me']) > 0) {
        $auth = true;
    }
    $state = $pdo_con->prepare("INSERT INTO chat values(?,?,?,NOW())");
    $result = $state->execute([($auth ? $_SESSION['me'] : $_GET['me']), ($auth ? $_SESSION['session'] : $_GET['to']), $chat]);
    echo $result;
}
function removeMe($you = "", $app = false)
{
    global $pdo_con;
    //This code was same with logout function

    //Delete connected first
    if (isset($_SESSION['me']) || $you != "") {
        $state = $pdo_con->prepare("DELETE FROM connected where connected_username=?");
        $result  = $state->execute([($you != "" ? $you : $_SESSION['me'])]);
        if ($you == "") {
            $state = $pdo_con->prepare("DELETE FROM provider where username=?");
            $result  = $state->execute([($you != "" ? $you : $_SESSION['me'])]);
            if ($result) {
                echo ("<script>window.location.href='index.php';</script>");
            }

            //Control if execute in app
            session_destroy();
        } else {
            echo "OK";
        }

        insertAction(($you != "" ? "logoutuserapp" : "logoutuser"), ($you != "" ? $you : $_SESSION['me']) . "|" . $_SERVER['REMOTE_ADDR']);
    }
}
function getPermission($id, $provider = "")
{
    global $pdo_con;
    $provider = ($provider == "" ? $_GET['session'] : $provider);
    $state = $pdo_con->prepare("SELECT * FROM connected_permission where connected_username=? AND provider_username=?");
    $state->execute([$id, $provider]);
    $result = $state->fetchAll();
    return $result;
}
function connectedLength($connected_username)
{
    global $pdo_con;
    $state = $pdo_con->prepare("SELECT * FROM connected where connected_username=?");
    $state->execute([$connected_username]);
    $result = $state->fetchAll();
    return $state->rowCount();
}
function isPermissionExist($permission, $you = "", $provider = "")
{
    global $pdo_con;
    //0=only desktop;1=chat;2=file manager;3=command prompt;4=change permission
    $ok = false;
    $state = $pdo_con->prepare("SELECT * FROM connected_permission where connected_username=? AND allow_permission=? AND provider_username=?");
    $state->execute([($you != "" ? $you : $_SESSION['me']), $permission, $provider]);
    $result = $state->fetchAll();
    if ($state->rowCount() > 0) {
        $ok = true;
    }
    return $ok;
}
function getProviderPermission()
{
    global $pdo_con;
    $ok = false;
    $state = $pdo_con->prepare("SELECT * FROM provider where username=?");
    $state->execute([(isset($_GET['session']) ? $_GET['session'] : $_SESSION['session'])]);
    $result = $state->fetchAll();

    return ($state->rowCount() > 0 ? $result[0]['permission'] : "");
}
function createPermission($permission = "0", $you = "", $provider = "")
{
    global $pdo_con;
    //0=only desktop;1=chat;2=file manager;3=command prompt;4=change permission
    if ($you != "" || (isset($_SESSION['me']) && isset($_SESSION['session']))) {
        if (!isPermissionExist($permission, $you, $provider)) {
            $me = ($you != "" ? $you : $_SESSION['me']);
            $session = ($provider != "" ? $provider : $_SESSION['session']);


            $state = $pdo_con->prepare("INSERT INTO connected_permission values(?,?,?)");
            $state->execute([$me, $session, $permission]);
        } else {
            // echo "Data is exist" . $permission . "<br>";
            // echo isPermissionExist("0",$you, $provider);
        }
    }
}
function removePermission($permission = "0", $you = "", $provider = "")
{
    global $pdo_con;
    //0=only desktop;1=chat;2=file manager;3=command prompt;4=change permission
    $state = $pdo_con->prepare("delete from connected_permission where connected_username=? AND provider_username=? AND allow_permission=?");
    $state->execute([$you, $provider, $permission]);
}

function removeAll($id)
{
    //$id = provider id

    //Untuk tau kalau dari aplikasi atau tidak
    global $pdo_con;
    $state = $pdo_con->prepare("delete from connected where provider_username=?");
    $result = $state->execute([$id]);
    $state = $pdo_con->prepare("delete from user_request where username_to=?");
    $result = $state->execute([$id]);

    // if ($result){
    //     echo "OK";
    // }else{
    //     echo "NOT OK";
    // }
}

function insertConnected($user, $pass, $permission)
{
    global $pdo_con;
    global $skrg;
    $d = checkandUpdateProvider($user, $pass, $permission);
    if ($d) {
        echo "OK";
    } else {
        $state = $pdo_con->prepare("INSERT INTO provider values('$user', '$pass', '$permission', NULL)");
        $result = $state->execute();
        if ($result) {
            echo "OK";
        } else {
            echo "NOT OK";
        }
    }
}
function insertAction($tipe = "", $aksi = "")
{
    global $pdo_con;
    global $skrg;
    $tipe = ($tipe == "" ? $_GET['type'] : $tipe);
    if ($aksi == "") {
        $aksi = (isset($_GET['action']) ? $_GET['action'] : $_POST['action']);
    }
    $itsme = (isset($_GET['me']) ? $_GET['me'] : $_SESSION['me']);
    $tsession = (isset($_GET['session']) != null ? $_GET['session'] : $_SESSION['session']);


    //Ngecek jika data yang dikirimkan sama seperti sblumnya dan belum terproses
    $state = $pdo_con->prepare("SELECT * FROM user_request where data='" . $aksi . "' AND username='" . $itsme . "' AND username_to='" . $tsession . "'");
    $state->execute();
    $result = $state->fetchAll();
    if (!empty($result)) {
        if ($state->rowCount() > 0) {
            //-1 menandakan apabila sudah ada data yang dikirimkan namun belum terproses.
            echo "-1";
        }
    } else {
        $state = $pdo_con->prepare("INSERT INTO user_request values('" . random(10) . "', '" . $itsme . "', '" .  $tsession . "', '" . $tipe . "','" . $aksi . "', '$skrg')");
        $x = $state->execute();
        echo $x;
    }
}
function insertActionwithCustomSender($sender, $tipe = "", $aksi = "")
{
    global $pdo_con;
    global $skrg;
    $tipe = ($tipe == "" ? $_GET['type'] : $tipe);
    if ($aksi == "") {
        $aksi = (isset($_GET['action']) ? $_GET['action'] : $_POST['action']);
    }
    $itsme = $sender;
    $tsession = (isset($_GET['session']) != null ? $_GET['session'] : $_SESSION['session']);


    //Ngecek jika data yang dikirimkan sama seperti sblumnya dan belum terproses
    $state = $pdo_con->prepare("SELECT * FROM user_request where data='" . $aksi . "' AND username='" . $itsme . "' AND username_to='" . $tsession . "'");
    $state->execute();
    $result = $state->fetchAll();
    if (!empty($result)) {
        if ($state->rowCount() > 0) {
            //-1 menandakan apabila sudah ada data yang dikirimkan namun belum terproses.
            echo "-1";
        }
    } else {
        $state = $pdo_con->prepare("INSERT INTO user_request values('" . random(10) . "', '" . $itsme . "', '" .  $tsession . "', '" . $tipe . "','" . $aksi . "', '$skrg')");
        $x = $state->execute();
    }
}
function get_client_ip_env()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function changeNameConnected($newname)
{
    $okeh = false;
    if (isset($_SESSION['me'])) {
        global $pdo_con;
        global $skrg;
        $con_user = $_SESSION['me'];
        $state = $pdo_con->prepare("UPDATE connected set connected_username=? where connected_username=?");
        $result = $state->execute([$newname, $con_user]);

        if ($result) {
            //sendAction();
            $okeh = true;
        } else {
            echo $state->errorInfo()[2];
        }
        $_SESSION['me'] = $newname;
    }
    return $okeh;
    //api.php?sendAction&type=fm&action=
}


function insertCon($con_user, $prov_user, $browser, $ip)
{

    $okeh = false;
    global $pdo_con;
    global $skrg;
    $state = $pdo_con->prepare("INSERT INTO connected values(?, ?, ?, ?, ?, ?)");
    $result = $state->execute([$con_user, $prov_user, $skrg, $skrg, $ip, $browser]);
    if ($result) {
        //sendAction();
        $okeh = true;
    } else {
        echo $state->errorInfo()[2];
    }
    return $okeh;
    //api.php?sendAction&type=fm&action=
}

function checkandUpdateProvider($user, $pass, $permission)
{
    //true menandakan ada di database, dan sudah diupdate password
    //false menandakan tidak ada di database, oleh karena itu harus dimasukkan terlebih dahulu datanya
    global $pdo_con;
    global $skrg;
    $indikator = false;

    $state = $pdo_con->prepare("SELECT * FROM provider WHERE username='" . $user . "'");
    $state->execute();
    $result = $state->fetchAll();
    if (!empty($result)) {
        foreach ($result as $row) {
            if ($pass != $row['password']) {
                $state2 = $pdo_con->prepare("UPDATE provider set password='" . $pass . "' WHERE username='" . $user . "'");
                $result2 = $state2->execute();
                if ($result2) {
                    $indikator = true;
                }
            } else {
                $indikator = true;
            }
            if ($permission != "" && $permission != $row['permission']) {
                $state2 = $pdo_con->prepare("UPDATE provider set permission='" . $permission . "' WHERE username='" . $user . "'");
                $result2 = $state2->execute();
                if ($result2) {
                    $indikator = true;
                }
            } else {
                $indikator = true;
            }
        }
    } else {
        $indikator = false;
    }
    return $indikator;
}
