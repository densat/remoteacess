<?php
session_start();
require_once("koneksi.php");

	if (isset($_GET['logout'])){
		session_destroy();
		$state = $pdo_con->prepare("DELETE FROM connected where connected_username='" . $_SESSION['me'] ."'");
		$result = $state->execute();
		if ($result){
			echo "<script>window.location.href='index.php';</script>";
		}

	}
	if (isset($_SESSION['session'])){
		$state = $pdo_con->prepare("SELECT * FROM provider where username='" . $_SESSION['session'] ."'");
		$state->execute();
		$result = $state->fetchAll();
		$gambar = $result[0]["screen"];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Remote Desktop - Administrator - <?php echo $_SESSION['me'] ?></title>
	<script type="text/javascript" src="js/1.3/jquery.min.js"></script>
	<script src="js/1.10.4/jquery-ui.js"></script>
	<script src="js/jquery-1.10.2.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="awesome/css/all.css">
</head>
<script>
var hide = 0;
function toggleMenu(){
    if (hide == 0){
        document.getElementsByTagName('li')[0].style="display:none;";
        document.getElementsByTagName('li')[1].style="display:none;";
        document.getElementsByTagName('li')[2].style="display:none;";
				document.getElementsByTagName('li')[3].style="display:none;";
				document.getElementsByTagName('li')[4].style="display:none;";
				document.getElementsByTagName('ul')[0].style="left:50%;opacity:0.6";
				
        
        document.getElementById('toggle').setAttribute("class", "fas fa-angle-down");
        hide = 1;
    }else{
        document.getElementsByTagName('li')[0].style="";
        document.getElementsByTagName('li')[1].style="";
        document.getElementsByTagName('li')[2].style="";
				document.getElementsByTagName('li')[3].style="";
				document.getElementsByTagName('li')[4].style="";
        document.getElementsByTagName('ul')[0].style="left:32%";
        document.getElementById('toggle').setAttribute("class", "fas fa-angle-up");
        hide = 0;
    }
}
</script>
<style>
body{
	margin:0 auto;
	font-family: 'FontAwesome'!important;
    background-color:black;
}
.menu{
	position: absolute;
    left: 50%;
    margin:0 auto;
    color:white;
    background-color:white;
    color:black;
    border-radius:2px;
}
ul {
    transition-duration: 0.5s;
	position: absolute;
	left:30%;
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: white;
		opacity:0.8;
	-moz-user-select: none;
	-webkit-user-select: none;
	user-select: none;
	user-drag: none; 
	-webkit-user-drag: none;
	-ms-user-select: none;
}

li {
  float: left;
  border-right:1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color:skyblue;
  text-align: center;
  padding: 8px 10px;
  text-decoration: none;
}
li:nth-child(5){
	background-color:red;
}
li:nth-child(5) a{
	color:white;
}
li a:hover:not(.active) {
  background-color: grey;
}

.active {
  background-color: #4CAF50;
}

#gambar{
	pointer-events: none;
	-moz-user-select: none;
	-webkit-user-select: none;
	user-select: none;
	user-drag: none; 
	-webkit-user-drag: none;
	-ms-user-select: none;
}
</style>

<script>
		function fe(id){
			if (id == 1){
				window.open("window_fe.html", "_blank", "width=700,height=500");
			}else if (id == 2){
				window.open("window_cmd.html", "_blank", "width=700,height=500");
			}
        }
		</script>
<script>
$("#gambar").mousedown(function(){
    return false;
});
setInterval(function() {
                $.get("api.php?getScreenshot", function (result) {
                    if (result != ""){
                        $('#gambar').attr("src", "data:image/png;base64,"+result);
                    }
                });
        }, 200);
</script>
<body onload="toggleMenu()">

<ul>
  <li><a onclick="fe(1)"><i class="fas fa-folder" style="padding-right:5px;color:skyblue"></i>File Manager</a></li>
  <li><a onclick="fe(2)"><i class="fas fa-code" style="padding-right:5px;color:skyblue"></i>Command Prompt</a></li>
  <li><a href="#contact"><i class="fas fa-comment" style="padding-right:5px;color:skyblue"></i>Chat</a></li>
	<li><a href="#heee"><i class="fas fa-redo-alt" style="padding-right:5px;color:skyblue"></i>Last Update</a></li>
	<li><a href="?logout"><i class="fas fa-redo-alt" style="padding-right:5px;color:skyblue"></i>End Session</a></li>
  <li style="float:right" onclick="toggleMenu()"><a href="#about"><i id="toggle" class="fas fa-angle-up" style="padding-right:5px;color:skyblue"></i></a></li>
</ul>

<div class="isi">
	<center>
	<img id="gambar" src="data:image/png;base64,<?php echo $gambar ?>" >
	</img>
</center>
</div>
<footer>
</footer>
</body>
</html>

<?php
	}
	else{
		echo "<script>window.location.href='index.php'</script>";
	}
function check_base64_image($base64) {
    $img = imagecreatefromstring(base64_decode($base64));
    if (!$img) {
        return false;
    }

    imagepng($img, 'tmp.png');
    $info = getimagesize('tmp.png');

    unlink('tmp.png');

    if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
        return true;
    }

    return false;
}
?>